ARG BUILD_OS=linux
ARG BUILD_ARCH=amd64
ARG BUILD_PLATFORM=linux/amd64
ARG BUILD_ARM_VERSION=''

# Build image
FROM golang:1.17 as build-env
ARG BUILD_OS
ARG BUILD_ARCH
ARG BUILD_PLATFORM
ARG BUILD_ARM_VERSION

WORKDIR /go/src/app
COPY . /go/src/app

RUN go get -d -v ./...

RUN echo "GOOS=${BUILD_OS} GOARCH=${BUILD_ARCH} GOARM=${BUILD_ARM_VERSION} BUILD_PLATFORM=${BUILD_PLATFORM}"
RUN GOOS=${BUILD_OS} GOARCH=${BUILD_ARCH} GOARM=${BUILD_ARM_VERSION} CGO_ENABLED=0 go build -ldflags="-s -w" -o /go/bin/app

# Final image
FROM --platform=${BUILD_PLATFORM} busybox:1.34.0

COPY --from=build-env /go/bin/app /
COPY --from=build-env /etc/ssl/certs /etc/ssl/certs

CMD ["/app"]
