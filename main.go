package main

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"os"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
)

type C02Data struct {
	CountryCode string `json:"countryCode"`
	Data        struct {
		CarbonIntensity      *float64  `json:"carbonIntensity,omitempty"`
		Datetime             time.Time `json:"datetime"`
		FossilFuelPercentage *float64  `json:"fossilFuelPercentage,omitempty"`
	} `json:"data"`
	Status string `json:"status"`
	Units  struct {
		CarbonIntensity string `json:"carbonIntensity"`
	} `json:"units"`
}

type result struct {
	country string
	data    C02Data
	err     error
}

func main() {
	client := influxdb2.NewClient(os.Getenv("INFLUXDB_HOST"), os.Getenv("INFLUXDB_TOKEN"))
	writeAPI := client.WriteAPI(os.Getenv("INFLUXDB_ORG"), os.Getenv("INFLUXDB_BUCKET"))

	countries := []string{"SE", "GB", "DE"}
	allPassed := true
	fmt.Printf("%s Gathering Co2 for: %s\n", logPrefix(), countries)

	for _, country := range countries {
		result := getLatestCo2DataForCountry(country)
		err := writeResultToDB(writeAPI, result)

		if err != nil {
			fmt.Printf("%s %+v\n", logPrefix(), err)
			allPassed = false
		}

		time.Sleep(15 * time.Second) // API rate limit
	}

	writeAPI.Flush()
	client.Close()
	fmt.Printf("%s Finished gathering metrics\n", logPrefix())
	time.Sleep(5 * time.Second) // Allows logger to parse logs

	if allPassed {
		fmt.Printf("%s All requests were successfull\n", logPrefix())
		os.Exit(0)
	} else {
		fmt.Printf("%s Some or all requests failed\n", logPrefix())
		os.Exit(1)
	}
}

func writeResultToDB(writeAPI api.WriteAPI, result result) error {
	if result.err == nil {
		country := string(result.data.CountryCode)
		carbonIntensity := int(math.Round(*result.data.Data.CarbonIntensity))
		carbonIntensityUnit := string(result.data.Units.CarbonIntensity)
		fossilFuelPercentage := float64(*result.data.Data.FossilFuelPercentage)
		fossilFuelPercentageUnit := "%"

		carbonIntensityPoint := influxdb2.NewPointWithMeasurement("co2").
			AddTag("country", country).
			AddTag("unit", carbonIntensityUnit).
			AddTag("source", "co2signal").
			AddField("carbonIntensity", carbonIntensity).
			SetTime(result.data.Data.Datetime)

		writeAPI.WritePoint(carbonIntensityPoint)

		fossilFuelPercentagePoint := influxdb2.NewPointWithMeasurement("co2").
			AddTag("country", country).
			AddTag("unit", fossilFuelPercentageUnit).
			AddTag("source", "co2signal").
			AddField("fossilFuelPercentage", fossilFuelPercentage).
			SetTime(result.data.Data.Datetime)

		writeAPI.WritePoint(fossilFuelPercentagePoint)

		fmt.Printf("%s carbonIntensity: %d%s, fossilFuelPercentage: %f%s, country: %s\n, time: %s\n", logPrefix(), carbonIntensity, carbonIntensityUnit, fossilFuelPercentage, fossilFuelPercentageUnit, country, result.data.Data.Datetime.Format(time.RFC3339))

		return nil
	}

	return result.err
}

func getLatestCo2DataForCountry(country string) result {
	client := &http.Client{}
	request, _ := http.NewRequest("GET", fmt.Sprintf("https://api.co2signal.com/v1/latest?countryCode=%s", country), nil)
	request.Header.Add("auth-token", os.Getenv("CO2SIGNAL_TOKEN"))

	response, err := client.Do(request)

	if err != nil {
		return result{country: country, err: err}
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return result{country: country, err: fmt.Errorf("co2signal: Latest data returned the status code: %d for country: %s", response.StatusCode, country)}
	}

	var co2Data C02Data

	err = json.NewDecoder(response.Body).Decode(&co2Data)

	if err != nil {
		return result{country: country, err: err}
	}

	if co2Data.Status != "ok" {
		return result{country: country, err: fmt.Errorf("co2signal: Latest data did not return 'ok' for country: %s", country)}
	}

	if co2Data.Data.CarbonIntensity == nil {
		return result{country: country, err: fmt.Errorf("co2signal: No carbon intenisty returned for country: %s", country)}
	}

	return result{country: country, data: co2Data}
}

func logPrefix() string {
	return fmt.Sprintf("[%s]", time.Now().Format(time.RFC3339))
}

func log(level string, component string, message string) {
	fmt.Printf("ts=%s level=%s component=%s message=%s\n", time.Now().Format(time.RFC3339), level, component, message)
}
