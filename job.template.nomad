job "metrics-energy-co2signal" {
  name = "CO2Signal Metrics"
  type = "batch"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"
  namespace = "energy"

  periodic {
    cron = "5 * * * *"
    prohibit_overlap = true
    time_zone = "[[ .defaultTimezone ]]"
  }

  reschedule {
    attempts  = 5
    interval = "1h"
    delay = "30s"
    delay_function = "exponential"
    max_delay = "120s"
    unlimited = false
  }

  vault {
    policies = ["api-co2signal", "job-metrics-energy-co2signal"]
  }

  group "co2signal" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "co2signal-metrics"
      tags = ["app"]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }

            upstreams {
              destination_name = "loki"
              local_bind_port = 3100
            }
          }
        }
      }
    }

    task "api" {
      driver = "docker"

      config {
        image = "[[ .jobDockerImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]

          {{ with secret "jobs/metrics-energy-co2signal/tasks/api/influxdb" }}
          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_ORG={{ index .Data.data "organisation" }}
          INFLUXDB_TOKEN={{ index .Data.data "token" }}
          INFLUXDB_BUCKET={{ index .Data.data "bucket" }}
          {{ end }}

          {{ with secret "api/co2signal/co2" }}
          CO2SIGNAL_TOKEN={{ index .Data.data "token" }}
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
