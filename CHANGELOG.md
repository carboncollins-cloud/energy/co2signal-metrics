# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2022-05-16]

### Removed
- Promtail log shipper as it has been superceeded by the loggin agent

## [2022-05-07]

### Changed
- Moved to new repo group
