# Metrics Energy CO2Signal

[[_TOC_]]

## Description

Gathers metrics from [CO2Signal](https://co2signal.com/) once every hour
